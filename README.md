# Installation
### Node packages
`npm install`

### Redis
`brew install redis`

# General Terms and Concepts
### Isomorphic
Code that is run on both the client and the server. Using React and node we are able to share a good bit of code between the server and the client. Intial state of the page is rendered on the server and then hooked up to React. React can manage page changes from there.

### [React-router](https://github.com/ReactTraining/react-router/)
Routing library for react that allows us to manage routing on both the client and the server. Routes managed in `src/common/routes.js`.

### [Redux](http://redux.js.org/)
Uni-directional data flow with a single managed store. 

##### Store
Repository for application state and data. The store is a single javascript object.

##### Actions
`src/common/actions/index.js`

Plain javascript object payloads that are `dispatch`ed to send information to the store. The store only receives informatino through an action `dispatch`.

##### Reducers
`src/common/reducers/index.js`

Functionally pure functions that calculate the new state after handling actions. State is not mutated but upon calculation from action payload a new state is returned.

##### Containers and components
Containers are *smart* and dispatch actions. Components are *dumb* and are passed action handlers as `props` from their parent container. In this way components can be shared. Both containers and components are found in `src/common/components/<Container>/*`. By convention containers are in the `index.js` and components are named by their function.

# Filesystem Layout
### Server-side Code
`src/server/*`

### Client-side Code
HTML template: `src/client/index.handlebars`.

Javascript entry point: `src/client/index.js`.

Statics: `public/*`

# Local development
`npm run dev`
and go to link http://localhost:3000/dashboard in browser for dashboard.

This will start the node server, redis server, and watch for file changes. Changes to files should recompile code and automatically update browser.

### Local Configuration
`src/server/local_config.js`
Use this file to change values that are set for each domain on the server. Right now the only thing to change is the login_domain to test keytrain and careerready101 differences.