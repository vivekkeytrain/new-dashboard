'use strict';

import React from 'react';

const action = require('../../src/common/actions/index')

test('Login using credentials vivek-admin, k3ytr@!n', () => {
	expect(do_signin('vivek-admin', 'k3ytr@!n').toBe('Successful Login'));
})

test('Testing fetch password reset request with fetchPasswordResetRequest', () => {
	expect(fetchPasswordResetRequest('vivek-admin').toBe('{ type: FETCH_PASSWORD_RESET_REQUEST, vivek-admin }'));
})

