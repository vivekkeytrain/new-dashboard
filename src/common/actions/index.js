import request from "superagent";

/*  Action types    */
export const CHANGE_ACTION = "CHANGE_ACTION";

/*  Async action types  */
export const FETCH_SIGNIN_REQUEST = "FETCH_SIGNIN_REQUEST";
export const FETCH_SIGNIN_FAILURE = "FETCH_SIGNIN_FAILURE";
export const FETCH_SIGNIN_SUCCESS = "FETCH_SIGNIN_SUCCESS";

export const FETCH_SIGNUP_REQUEST = "FETCH_SIGNUP_REQUEST";
export const FETCH_SIGNUP_FAILURE = "FETCH_SIGNUP_FAILURE";
export const FETCH_SIGNUP_SUCCESS = "FETCH_SIGNUP_SUCCESS";

export const FETCH_PASSWORD_RESET_REQUEST = "FETCH_PASSWORD_RESET_REQUEST";
export const FETCH_PASSWORD_RESET_FAILURE = "FETCH_PASSWORD_RESET_FAILURE";
export const FETCH_PASSWORD_RESET_SUCCESS = "FETCH_PASSWORD_RESET_SUCCESS";

export const FETCH_USER_DATA_REQUEST = "FETCH_USER_DATA_REQUEST";
export const FETCH_USER_DATA_FAILURE = "FETCH_USER_DATA_FAILURE";
export const FETCH_USER_DATA_SUCCESS = "FETCH_USER_DATA_SUCCESS";

export const FETCH_STUDENT_DASHBOARD_HOME_REQUEST = "FETCH_STUDENT_DASHBOARD_HOME_REQUEST";
export const FETCH_STUDENT_DASHBOARD_HOME_FAILURE = "FETCH_STUDENT_DASHBOARD_HOME_FAILURE";
export const FETCH_STUDENT_DASHBOARD_HOME_SUCCESS = "FETCH_STUDENT_DASHBOARD_HOME_SUCCESS";

/*  Actions     */
export function changeAction(action) {
  return { type: CHANGE_ACTION, action };
}

/*  Async actions   */
function fetchSigninRequest(username, password) {
  return { type: FETCH_SIGNIN_REQUEST, username, password };
}

function fetchSigninFailure(ex) {
  return { type: FETCH_SIGNIN_FAILURE, ex };
}

function fetchSigninSuccess(user_data, messages) {
  return { type: FETCH_SIGNIN_SUCCESS, user_data, messages };
}

//export function do_signin(username, password) {
function do_signin(username, password) {
  console.log("do_signin", username, password);

  return dispatch => {
    dispatch(fetchSigninRequest(username, password));

    return request
      .post("/api/login")
      .send({ username, password })
      .then(response => response.body, ex => dispatch(fetchSigninFailure(ex)))
      .then(json => dispatch(fetchSigninSuccess(json[0][0], json[1] || [])))
      .catch(ex => dispatch(fetchSigninFailure(ex)));
  };
}

function fetchPasswordResetRequest(username) {
  return { type: FETCH_PASSWORD_RESET_REQUEST, username };
}

function fetchPasswordResetFailure(ex) {
  return { type: FETCH_PASSWORD_RESET_FAILURE, ex };
}

function fetchPasswordResetSuccess(status) {
  return { type: FETCH_PASSWORD_RESET_SUCCESS, status };
}

export function sendPasswordReset(username) {
  console.log("sendPasswordReset", username);

  return dispatch => {
    dispatch(fetchPasswordResetRequest(username));

    return request
      .post("/api/password/send")
      .send({ username })
      .then(
        response => response.body,
        ex => dispatch(fetchPasswordResetFailure(ex))
      )
      .then(json => dispatch(fetchPasswordResetSuccess(json.status)))
      .catch(ex => dispatch(fetchPasswordResetFailure(ex)));
  };
}

function fetchUserDataRequest(luid) {
  return { type: FETCH_USER_DATA_REQUEST, luid };
}

function fetchUserDataFailure(ex) {
  return { type: FETCH_USER_DATA_FAILURE, ex };
}

function fetchUserDataSuccess(user_data) {
  return { type: FETCH_USER_DATA_SUCCESS, user_data };
}

export function fetchUserData(luid) {
  return dispatch => {
    dispatch(fetchUserDataRequest(luid));

    return request
      .post("/api/password/data")
      .send({ luid })
      .then(response => response.body, ex => dispatch(fetchUserDataFailure(ex)))
      .then(json => dispatch(fetchUserDataSuccess(json)))
      .catch(ex => dispatch(fetchUserDataFailure(ex)));
  };
}

function fetchStudentDashboardHomeRequest(entity_tag) {
  return { type: FETCH_STUDENT_DASHBOARD_HOME_REQUEST, entity_tag };
}

function fetchStudentDashboardHomeFailure(ex) {
  return { type: FETCH_STUDENT_DASHBOARD_HOME_FAILURE, ex };
}

function fetchStudentDashboardHomeSuccess(user_data) {
  return { type: FETCH_STUDENT_DASHBOARD_HOME_SUCCESS, user_data };
}

export function fetchStudentDashboardHome(entity_tag) {
  return dispatch => {
    dispatch(fetchStudentDashboardHomeRequest(entity_tag));

    return request
      .get(
        `http://sdapi.actkeytrain.com/dbwebapi/dbo.api_sp_get_student_dashboard_home/json?entity_tag=${entity_tag}`
      )
      .then(
        response => response.body,
        ex => dispatch(fetchStudentDashboardHomeFailure(ex))
      )
      .then(json =>
        dispatch(fetchStudentDashboardHomeSuccess(json.ResultSets[0])))
      .catch(ex => dispatch(fetchStudentDashboardHomeFailure(ex)));
  };
}
