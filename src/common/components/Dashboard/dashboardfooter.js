import React, { PropTypes } from "react";

const Dashboardfooter = ({ orgzname }) => (
    <div>
        <div className="row gray-darker-bg pad-tb-s30 p-none">
            <div className="container">
                <div className="col-md-12 gray font-size-small text-center">
                    <p>Copyright © 2017  ACT, Inc</p>
                </div>
            </div>
        </div>
    </div>
);

Dashboardfooter.propTypes = { orgzname: PropTypes.string };

export default Dashboardfooter;
