import React, { Component } from "react";
import { connect } from "react-redux";

import Dashboardfooter from "src/common/components/Dashboard/dashboardfooter";
import Dashboardheader from "src/common/components/Dashboard/dashboardheader";

//require("./profile.scss");
//import style from "./profile.scss";

class App extends Component {
    render() {
        console.log("render dashboard index");

        return (
            <div className="container body">
                <Dashboardheader />
					<div className="right_col" role="main">
          <div className="row tile_count">
            <div className="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
              <span className="count_top"><i className="fa fa-mortar-board"></i>Lessions Completed</span>
              <div className="count">4</div>
              <span className="count_bottom"><i className="green">4% </i> From last Week</span>
            </div>
            <div className="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
              <span className="count_top"><i className="fa fa-clock-o"></i> Average Time</span>
              <div className="count">123.50</div>
              <span className="count_bottom"><i className="green"><i className="fa fa-sort-asc"></i>3% </i> From last Week</span>
            </div>
            <div className="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
              <span className="count_top"><i className="fa fa-user"></i> Total Topics</span>
              <div className="count green">7</div>
              <span className="count_bottom"><i className="green"><i className="fa fa-sort-asc"></i>34% </i> From last Week</span>
            </div>
            <div className="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
              <span className="count_top"><i className="fa fa-file-text"></i> Pages Completed</span>
              <div className="count">4,567</div>
              <span className="count_bottom"><i className="red"><i className="fa fa-sort-desc"></i>12% </i> From last Week</span>
            </div>
          </div>
		  <div className="row">
			<div className="col-md-4 col-sm-4 col-xs-12 hidden">
              <div className="x_panel tile fixed_height_320">
                <div className="x_title">
                  <h2>Applied Mathematics</h2>
                  <ul className="nav navbar-right panel_toolbox">
                    <li><a className="collapse-link"><i className="fa fa-chevron-up"></i></a>
                    </li>
                    <li className="dropdown">
                      <a href="#" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i className="fa fa-wrench"></i></a>
                      <ul className="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a className="close-link"><i className="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div className="clearfix"></div>
                </div>
                <div className="x_content">
                  <h4>Gauge of completeness</h4>
                  <div className="widget_summary">
					<svg id="fillgauge2" width="90%" height="200" onclick="gauge2.update(NewValue());"></svg>
				  </div>

                </div>
              </div>
            </div>
			<div className="col-md-4 col-sm-4 col-xs-12">
              <div className="x_panel tile fixed_height_320">
                <div className="x_title">
                  <h2>Applied Mathematics</h2>
                  <ul className="nav navbar-right panel_toolbox">
                    <li><a className="collapse-link"><i className="fa fa-chevron-up"></i></a>
                    </li>
                    <li className="dropdown">
                      <a href="#" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i className="fa fa-wrench"></i></a>
                      <ul className="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a className="close-link"><i className="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div className="clearfix"></div>
                </div>
                <div className="x_content">
                  <h4>Gauge of completeness</h4>
                  <div className="widget_summary">
					<svg id="fillgauge3" width="90%" height="200" onclick="gauge3.update(NewValue());"></svg>
				  </div>

                </div>
              </div>
            </div>
			<div className="col-md-8 col-sm-8 col-xs-12">
              <div className="x_panel tile ">
                <div className="x_title">
                  <h2>Level of Completion</h2>
                  <ul className="nav navbar-right panel_toolbox">
                    <li><a className="collapse-link"><i className="fa fa-chevron-up"></i></a>
                    </li>
                    <li className="dropdown">
                      <a href="#" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i className="fa fa-wrench"></i></a>
                      <ul className="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a className="close-link"><i className="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div className="clearfix"></div>
                </div>
                <div className="x_content">
                  <div className="widget_summary">
					<div id="hor_bar_container" ></div>
				  </div>

                </div>
              </div>
            </div>
		  </div>
		  <br/>
          <div className="row">
            <div className="col-md-12 col-sm-12 col-xs-12">
              <div className="dashboard_graph">

                <div className="row x_title">
                  <div className="col-md-6">
                    <h3>Activity Stats <small>Daily Spent Minutes</small></h3>
                  </div>
                  <div className="col-md-6">
                    <div id="reportrange" className="pull-right" >
                      <i className="glyphicon glyphicon-calendar fa fa-calendar"></i>
                      <span>December 30, 2014 - January 28, 2015</span> <b className="caret"></b>
                    </div>
                  </div>
                </div>

                <div className="col-md-9 col-sm-9 col-xs-12">
                  <div id="chart_plot_01" className="demo-placeholder"></div>
                </div>
                <div className="col-md-3 col-sm-3 col-xs-12 bg-white">
                  <div className="x_title">
                    <h2>Up next</h2>
                    <div className="clearfix"></div>
                  </div>
					
                  <div className="col-md-12 col-sm-12 col-xs-6">
                    <div>
                      <p>Applied Maths - Level 6</p>
                      <div className="">
                        <div className="progress progress_sm">
                          <div className="progress-bar bg-green" role="progressbar" data-transitiongoal="80"></div>
                        </div>
                      </div>
                    </div>
                    <div>
                      <p>Locating Information - Level 3</p>
                      <div className="">
                        <div className="progress progress_sm" >
                          <div className="progress-bar bg-red" role="progressbar" data-transitiongoal="60"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-12 col-sm-12 col-xs-6">
                    <div>
                      <p>Workplace Observation - Pretest</p>
                      <div className="">
                        <div className="progress progress_sm" >
                          <div className="progress-bar bg-purple" role="progressbar" data-transitiongoal="40"></div>
                        </div>
                      </div>
                    </div>
                    <div>
                      <p>Bill boards</p>
                      <div className="">
                        <div className="progress progress_sm" >
                          <div className="progress-bar bg-blue" role="progressbar" data-transitiongoal="50"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="clearfix"></div>
              </div>
            </div>

          </div>
          <br />
          <div className="row">


            <div className="col-md-4 col-sm-4 col-xs-12">
              <div className="x_panel tile fixed_height_320">
                <div className="x_title">
                  <h2>Subject Visited</h2>
                  <ul className="nav navbar-right panel_toolbox">
                    <li><a className="collapse-link"><i className="fa fa-chevron-up"></i></a>
                    </li>
                    <li className="dropdown">
                      <a href="#" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i className="fa fa-wrench"></i></a>
                      <ul className="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a className="close-link"><i className="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div className="clearfix"></div>
                </div>
                <div className="x_content">
                  <h4>Subject Visited on Act KeyTrain</h4>
                  <div className="widget_summary">
                    <div className="w_left w_25">
                      <span>Maths</span>
                    </div>
                    <div className="w_center w_55">
                      <div className="progress">
                        <div className="progress-bar bg-green" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" >
                          <span className="sr-only">60% Complete</span>
                        </div>
                      </div>
                    </div>
                    <div className="w_right w_20">
                      <span>123k</span>
                    </div>
                    <div className="clearfix"></div>
                  </div>

                  <div className="widget_summary">
                    <div className="w_left w_25">
                      <span>Physics</span>
                    </div>
                    <div className="w_center w_55">
                      <div className="progress">
                        <div className="progress-bar bg-green" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" >
                          <span className="sr-only">60% Complete</span>
                        </div>
                      </div>
                    </div>
                    <div className="w_right w_20">
                      <span>53k</span>
                    </div>
                    <div className="clearfix"></div>
                  </div>
                  <div className="widget_summary">
                    <div className="w_left w_25">
                      <span>Chemistry</span>
                    </div>
                    <div className="w_center w_55">
                      <div className="progress">
                        <div className="progress-bar bg-green" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" >
                          <span className="sr-only">60% Complete</span>
                        </div>
                      </div>
                    </div>
                    <div className="w_right w_20">
                      <span>23k</span>
                    </div>
                    <div className="clearfix"></div>
                  </div>
                  <div className="widget_summary">
                    <div className="w_left w_25">
                      <span>Computer</span>
                    </div>
                    <div className="w_center w_55">
                      <div className="progress">
                        <div className="progress-bar bg-green" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" >
                          <span className="sr-only">60% Complete</span>
                        </div>
                      </div>
                    </div>
                    <div className="w_right w_20">
                      <span>3k</span>
                    </div>
                    <div className="clearfix"></div>
                  </div>
                  <div className="widget_summary">
                    <div className="w_left w_25">
                      <span>Botony</span>
                    </div>
                    <div className="w_center w_55">
                      <div className="progress">
                        <div className="progress-bar bg-green" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" >
                          <span className="sr-only">60% Complete</span>
                        </div>
                      </div>
                    </div>
                    <div className="w_right w_20">
                      <span>1k</span>
                    </div>
                    <div className="clearfix"></div>
                  </div>

                </div>
              </div>
            </div>

			
            <div className="col-md-4 col-sm-4 col-xs-12">
              <div className="x_panel tile fixed_height_320 overflow_hidden">
                <div className="x_title">
                  <h2>Subject Usage</h2>
                  <ul className="nav navbar-right panel_toolbox">
                    <li><a className="collapse-link"><i className="fa fa-chevron-up"></i></a>
                    </li>
                    <li className="dropdown">
                      <a href="#" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i className="fa fa-wrench"></i></a>
                      <ul className="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a className="close-link"><i className="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div className="clearfix"></div>
                </div>
                <div className="x_content">
                  <table className="" >
                    <tr>
                      <th>
                        <p>Top 5</p>
                      </th>
                      <th>
                        <div className="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                          <p className="">Subject</p>
                        </div>
                        <div className="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                          <p className="">Progress</p>
                        </div>
                      </th>
                    </tr>
                    <tr>
                      <td>
                        <canvas className="canvasDoughnut" height="120" width="120" ></canvas>
						
                      </td>
                      <td>
                       
                      </td>
                    </tr>
                  </table>
                </div>
              </div>
            </div>


            <div className="col-md-4 col-sm-4 col-xs-12">
              <div className="x_panel tile fixed_height_320">
                <div className="x_title">
                  <h2>Quick Settings</h2>
                  <ul className="nav navbar-right panel_toolbox">
                    <li><a className="collapse-link"><i className="fa fa-chevron-up"></i></a>
                    </li>
                    <li className="dropdown">
                      <a href="#" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i className="fa fa-wrench"></i></a>
                      <ul className="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a className="close-link"><i className="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div className="clearfix"></div>
                </div>
                <div className="x_content">
                  <div className="dashboard-widget-content">
                    <ul className="quick-list">
                      <li><i className="fa fa-calendar-o"></i><a href="#">Settings</a>
                      </li>
                      <li><i className="fa fa-bars"></i><a href="#">Subscription</a>
                      </li>
                      <li><i className="fa fa-bar-chart"></i><a href="#">Auto Renewal</a> </li>
                      <li><i className="fa fa-line-chart"></i><a href="#">Achievements</a>
                      </li>
                      <li><i className="fa fa-bar-chart"></i><a href="#">Auto Renewal</a> </li>
                      <li><i className="fa fa-line-chart"></i><a href="#">Achievements</a>
                      </li>
                      <li><i className="fa fa-area-chart"></i><a href="#">Logout</a>
                      </li>
                    </ul>

                    <div className="sidebar-widget">
                        <h4>Profile Completion</h4>
                        <canvas width="150" height="80" id="chart_gauge_01" className="" ></canvas>
                        <div className="goal-wrapper">
                          <span id="gauge-text" className="gauge-value pull-left">0</span>
                          <span className="gauge-value pull-left">%</span>
                          <span id="goal-text" className="goal-value pull-right">100%</span>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>


          <div className="row">
            <div className="col-md-4 col-sm-4 col-xs-12">
              <div className="x_panel">
                <div className="x_title">
                  <h2>Recent Activities <small>Sessions</small></h2>
                  <ul className="nav navbar-right panel_toolbox">
                    <li><a className="collapse-link"><i className="fa fa-chevron-up"></i></a>
                    </li>
                    <li className="dropdown">
                      <a href="#" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i className="fa fa-wrench"></i></a>
                      <ul className="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a className="close-link"><i className="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div className="clearfix"></div>
                </div>
                <div className="x_content">
                  <div className="dashboard-widget-content">

                    <ul className="list-unstyled timeline widget">
                      <li>
                        <div className="block">
                          <div className="block_content">
                            <h2 className="title">
                                              <a>Applied Mathematics - Level 3</a>
                                          </h2>
                            <div className="byline">
                              <span>13 hours ago</span> by <a>Jane Smith</a>
                            </div>
                            <p className="excerpt">In and out. No progress. <a>Read&nbsp;More</a>
                            </p>
                          </div>
                        </div>
                      </li>
                      <li>
                        <div className="block">
                          <div className="block_content">
                            <h2 className="title">
                                              <a>Applied Mathematics - Level 3</a>
                                          </h2>
                            <div className="byline">
                              <span>13 hours ago</span> by <a>Jane Smith</a>
                            </div>
                            <p className="excerpt">2 topics completed.  <a>Read&nbsp;More</a>
                            </p>
                          </div>
                        </div>
                      </li>
                      <li>
                        <div className="block">
                          <div className="block_content">
                            <h2 className="title">
                                              <a>Applied Mathematics - Level 4</a>
                                          </h2>
                            <div className="byline">
                              <span>13 hours ago</span> by <a>Jane Smith</a>
                            </div>
                            <p className="excerpt">4 topics completed. Scored 93 on Final Quiz.  <a>Read&nbsp;More</a>
                            </p>
                          </div>
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>


            <div className="col-md-8 col-sm-8 col-xs-12">
              
              <div className="row">


                <div className="col-md-6 col-sm-6 col-xs-12">
                  <div className="x_panel">
                    <div className="x_title">
                      <h2>To Do List <small>Sample tasks</small></h2>
                      <ul className="nav navbar-right panel_toolbox">
                        <li><a className="collapse-link"><i className="fa fa-chevron-up"></i></a>
                        </li>
                        <li className="dropdown">
                          <a href="#" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i className="fa fa-wrench"></i></a>
                          <ul className="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a>
                            </li>
                            <li><a href="#">Settings 2</a>
                            </li>
                          </ul>
                        </li>
                        <li><a className="close-link"><i className="fa fa-close"></i></a>
                        </li>
                      </ul>
                      <div className="clearfix"></div>
                    </div>
                    <div className="x_content">

                      <div className="">
                        <ul className="to_do">
                          <li>
                            <p>
                              <input type="checkbox" className="flat" /> Complete 3 chaptors in Applied Maths</p>
                          </li>
                          <li>
                            <p>
                              <input type="checkbox" className="flat" /> Finish 4 levels in Locating Information</p>
                          </li>
                          <li>
                            <p>
                              <input type="checkbox" className="flat" /> Have IT fix the network printer</p>
                          </li>
                          <li>
                            <p>
                              <input type="checkbox" className="flat" /> Copy backups to offsite location</p>
                          </li>
                          <li>
                            <p>
                              <input type="checkbox" className="flat" /> Food truck fixie locavors mcsweeney</p>
                          </li>
                          <li>
                            <p>
                              <input type="checkbox" className="flat" /> Create email address for new intern</p>
                          </li>
                          <li>
                            <p>
                              <input type="checkbox" className="flat" /> Have IT fix the network printer</p>
                          </li>
                          <li>
                            <p>
                              <input type="checkbox" className="flat" /> Copy backups to offsite location</p>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-md-6 col-sm-6 col-xs-12">
                  <div className="x_panel">
                    <div className="x_title">
                      <h2>Daily active users <small>Sessions</small></h2>
                      <ul className="nav navbar-right panel_toolbox">
                        <li><a className="collapse-link"><i className="fa fa-chevron-up"></i></a>
                        </li>
                        <li className="dropdown">
                          <a href="#" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i className="fa fa-wrench"></i></a>
                          <ul className="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a>
                            </li>
                            <li><a href="#">Settings 2</a>
                            </li>
                          </ul>
                        </li>
                        <li><a className="close-link"><i className="fa fa-close"></i></a>
                        </li>
                      </ul>
                      <div className="clearfix"></div>
                    </div>
                    <div className="x_content">
                      <div className="row">
                        <div className="col-sm-12">
                          <div className="temperature"><b>Monday</b>, 07:30 AM
                            <span>F</span>
                            <span><b>C</b></span>
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-sm-4">
                          <div className="weather-icon">
                            <canvas height="84" width="84" id="partly-cloudy-day"></canvas>
                          </div>
                        </div>
                        <div className="col-sm-8">
                          <div className="weather-text">
                            <h2>Texas <br/><i>Partly Cloudy Day</i></h2>
                          </div>
                        </div>
                      </div>
                      <div className="col-sm-12">
                        <div className="weather-text pull-right">
                          <h3 className="degrees">23</h3>
                        </div>
                      </div>

                      <div className="clearfix"></div>

                      <div className="row weather-days">
                        <div className="col-sm-2">
                          <div className="daily-weather">
                            <h2 className="day">Mon</h2>
                            <h3 className="degrees">25</h3>
                            <canvas id="clear-day" width="32" height="32"></canvas>
                            <h5>15 <i>km/h</i></h5>
                          </div>
                        </div>
                        <div className="col-sm-2">
                          <div className="daily-weather">
                            <h2 className="day">Tue</h2>
                            <h3 className="degrees">25</h3>
                            <canvas height="32" width="32" id="rain"></canvas>
                            <h5>12 <i>km/h</i></h5>
                          </div>
                        </div>
                        <div className="col-sm-2">
                          <div className="daily-weather">
                            <h2 className="day">Wed</h2>
                            <h3 className="degrees">27</h3>
                            <canvas height="32" width="32" id="snow"></canvas>
                            <h5>14 <i>km/h</i></h5>
                          </div>
                        </div>
                        <div className="col-sm-2">
                          <div className="daily-weather">
                            <h2 className="day">Thu</h2>
                            <h3 className="degrees">28</h3>
                            <canvas height="32" width="32" id="sleet"></canvas>
                            <h5>15 <i>km/h</i></h5>
                          </div>
                        </div>
                        <div className="col-sm-2">
                          <div className="daily-weather">
                            <h2 className="day">Fri</h2>
                            <h3 className="degrees">28</h3>
                            <canvas height="32" width="32" id="wind"></canvas>
                            <h5>11 <i>km/h</i></h5>
                          </div>
                        </div>
                        <div className="col-sm-2">
                          <div className="daily-weather">
                            <h2 className="day">Sat</h2>
                            <h3 className="degrees">26</h3>
                            <canvas height="32" width="32" id="cloudy"></canvas>
                            <h5>10 <i>km/h</i></h5>
                          </div>
                        </div>
                        <div className="clearfix"></div>
                      </div>
                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        site_type: state.root.site_type,
        user_data: state.root.user_data,
        status: state.root.status
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {};
};

const ConnectedApp = connect(mapStateToProps, mapDispatchToProps)(App);

export default ConnectedApp;
