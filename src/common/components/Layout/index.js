import React from "react";

export default class extends React.Component {
  render() {
    // console.log('Layout render');
    return (
      <div id="wrapper">
        <div id="content">
          {this.props.children}
        </div>
        <div id="footer" className="small bg-gray">
          <div className="container">
            <ul className="list-inline hidden-print">
              <li>
                <span id="copyright">
                  © {new Date().getFullYear()} by ACT, Inc.
                </span>
              </li>
              <li>
                <a href="http://www.act.org/disclaimer.html" target="_blank">
                  Terms of Use
                </a>
              </li>
              <li>
                <a href="http://www.act.org/privacy.html" target="_blank">
                  Privacy Policy
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    );
  }
}
