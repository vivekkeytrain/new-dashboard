import React, { Component } from "react";
import { connect } from "react-redux";

import Dashboardfooter from "src/common/components/Dashboard/dashboardfooter";
import Dashboardheader from "src/common/components/Dashboard/dashboardheader";

//require("./profile.scss");
import style from "../Dashboard/profile.scss";

class App extends Component {
    render() {
        console.log("render lessons index");

        return (
            <div className="container body">
                <Dashboardheader />
                <div className="right_col" role="main">
					<div className="">
						<div className="page-title">
							<div className="title_left">
								<h3>My Lessons</h3>
							</div>

							<div className="title_right">
								<div className="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
									<div className="input-group">
										<input type="text" className="form-control" placeholder="Search for..." />
										<span className="input-group-btn">
											<button className="btn btn-default" type="button">Go!</button>
										</span>
									</div>
								</div>
							</div>
						</div>

						<div className="clearfix"></div>

						<div className="">
							<div className="col-md-12 col-sm-12 col-xs-12">
								<div className="x_panel">
									<div className="x_title">
										<h2><i className="fa fa-bars"></i> Lessons Sections</h2>
										<ul className="nav navbar-right panel_toolbox">
											<li>
												<a className="collapse-link"><i className="fa fa-chevron-up"></i></a>
											</li>
											<li className="dropdown">
												<a href="#" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
													<i className="fa fa-wrench"></i>
												</a>
												<ul className="dropdown-menu" role="menu">
													<li><a href="#">Settings 1</a></li>
													<li><a href="#">Settings 2</a></li>
												</ul>
											</li>
											<li><a className="close-link"><i className="fa fa-close"></i></a></li>
										</ul>
										<div className="clearfix"></div>
									</div>
									<div className="x_content">
										<div className="" role="tabpanel" data-example-id="togglable-tabs">
											<ul id="myTab" className="nav nav-tabs bar_tabs" role="tablist">
												<li role="presentation" className="active">
													<a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">
														<i className="fa fa-pencil" ></i> To Do
													</a>
												</li>
												<li role="presentation" className="">
													<a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">
														<i className="fa fa-check-square-o" ></i> Done
													</a>
												</li>
												<li role="presentation" className="">
													<a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">
														<i className="fa fa-calendar" ></i> Timeline
													</a>
												</li>
												<li role="presentation" className="">
													<a href="#tab_content4" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">More...</a>
												</li>
											</ul>
											<div id="myTabContent" className="tab-content">
												<div role="tabpanel" className="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
													<div className="col-md-12 col-sm-12 col-xs-12">
														<div className="x_panel">
															<div className="x_title">
																<h2>To Do List <small>Sample tasks</small></h2>
																<ul className="nav navbar-right panel_toolbox">
																	<li>
																		<a className="collapse-link"><i className="fa fa-chevron-up"></i></a>
																	</li>
																	<li className="dropdown">
																		<a href="#" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i className="fa fa-wrench"></i></a>
																		<ul className="dropdown-menu" role="menu">
																			<li><a href="#">Settings 1</a>
																			</li>
																			<li><a href="#">Settings 2</a>
																			</li>
																		</ul>
																	</li>
																	<li>
																		<a className="close-link"><i className="fa fa-close"></i></a>
																	</li>
																</ul>
																<div className="clearfix"></div>
															</div>
															<div className="x_content">
																<div className="">
																	<ul className="to_do">
																	  <li>
																		<p>
																		  <input type="checkbox" className="flat" /> Complete 3 chaptors in Applied Maths</p>
																	  </li>
																	  <li>
																		<p>
																		  <input type="checkbox" className="flat" /> Finish 4 levels in Locating Information</p>
																	  </li>
																	  <li>
																		<p>
																		  <input type="checkbox" className="flat" /> Have IT fix the network printer</p>
																	  </li>
																	  <li>
																		<p>
																		  <input type="checkbox" className="flat" /> Copy backups to offsite location</p>
																	  </li>
																	  <li>
																		<p>
																		  <input type="checkbox" className="flat" /> Food truck fixie locavors mcsweeney</p>
																	  </li>
																	  <li>
																		<p>
																		  <input type="checkbox" className="flat" /> Create email address for new intern</p>
																	  </li>
																	  <li>
																		<p>
																		  <input type="checkbox" className="flat" /> Have IT fix the network printer</p>
																	  </li>
																	  <li>
																		<p>
																		  <input type="checkbox" className="flat" /> Copy backups to offsite location</p>
																	  </li>
																	</ul>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div role="tabpanel" className="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
												  <p>Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid. Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four loko farm-to-table craft beer twee. Qui photo
													booth letterpress, commodo enim craft beer mlkshk aliquip</p>
												</div>
												<div role="tabpanel" className="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
												  <p>xxFood truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid. Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four loko farm-to-table craft beer twee. Qui photo
													booth letterpress, commodo enim craft beer mlkshk </p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						<div className="clearfix"></div>
					</div>
					<div className="clearfix"></div>
				</div>
            </div>
        );
    }
	
}

  
const mapStateToProps = state => {
    return {
        site_type: state.root.site_type,
        user_data: state.root.user_data,
        status: state.root.status
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {};
};

const ConnectedApp = connect(mapStateToProps, mapDispatchToProps)(App);

export default ConnectedApp;