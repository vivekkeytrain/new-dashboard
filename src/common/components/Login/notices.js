import React, { PropTypes } from "react";

import classNames from "classnames";

const Notices = ({ notices }) => {
  if (!notices) return <span />;

  return (
    <span>
      {notices.map((notice, i) => (
        <div className={classNames({
            "login-message": true,
            warning: notice.type == "warning"
          })} key={i}>
          {notice.message}
        </div>
      ))}
    </span>
  );
};

Notices.propTypes = {
  notices: PropTypes.arrayOf(PropTypes.shape({
      type: PropTypes.string.isRequired,
      message: PropTypes.string.isRequired
    }).isRequired)
};

export default Notices;
