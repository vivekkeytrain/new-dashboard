import React, { PropTypes } from "react";

const SelfSignupTop = ({ selfSignup, action, changeAction }) => {
  console.log("SelfSignupTop", selfSignup, action);
  if (selfSignup) {
    if (action === "signin") {
      return (
        <div>
          <p>
            Don't have an account yet?{" "}
            <a href="#" onClick={e => changeAction("signup", e)}>Sign up!</a>
          </p>
        </div>
      );
    } else {
      return (
        <div>
          <p>
            Already have an account?{" "}
            <a href="#" onClick={e => changeAction("signin", e)}>Sign in</a>
          </p>
        </div>
      );
    }
  } else {
    return <span />;
  }
};

SelfSignupTop.propTypes = {
  selfSignup: PropTypes.object,
  action: PropTypes.string.isRequired,
  changeAction: PropTypes.func.isRequired
};

export default SelfSignupTop;
