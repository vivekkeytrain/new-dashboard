import React, { PropTypes } from "react";

const Welcome = ({ orgzname }) => (
  <span>
    <span
      style={{
        fontStyle: "italic",
        fontFamily: "Garamond, Arial",
        paddingRight: "5px"
      }}
    >
      for
    </span>
    {orgzname}
  </span>
);

Welcome.propTypes = { orgzname: PropTypes.string.isRequired };

export default Welcome;
