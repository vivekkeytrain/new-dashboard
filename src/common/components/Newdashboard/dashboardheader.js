import React, { PropTypes } from "react";

const Dashboardheader = ({ propUserName }) => (
    <div className="container body">
		<div className="main_container">
			<div className="col-md-3 left_col">
				<div className="left_col scroll-view">
					<div className="navbar nav_title" >
						<a href="index.html" className="site_title">
							<span>
								<img src="/img/keytrain_rev.png" alt="Logo" />
							</span>
						</a>
					</div>

					<div className="clearfix"></div>

					<div className="profile clearfix">
						<div className="profile_pic">
							<img src="/img/img.jpg" alt="User Image" className="img-circle profile_img" />
						</div>
						<div className="profile_info">
							<span>Welcome,</span>
							<h2>Walter Sobchak</h2>
						</div>
					</div>

					<br />

					<div id="sidebar-menu" className="main_menu_side hidden-print main_menu">
						<div className="menu_section">
							<h3>General</h3>
							<ul className="nav side-menu">
								<li>
									<a>
										<i className="fa fa-home"></i> Home 
										<span className="fa fa-chevron-down"></span>
									</a>
									<ul className="nav child_menu">
										<li><a href="index.html">Dashboard</a></li>
									</ul>
								</li>
								<li>
									<a>
										<i className="fa fa-edit"></i> Lessons 
										<span className="fa fa-chevron-down"></span>
									</a>
									<ul className="nav child_menu">
										<li>
											<a href="lessons.html">My Lessons</a>
										</li>
									</ul>
								</li>
								<li>
									<a>
										<i className="fa fa-desktop"></i> My States 
										<span className="fa fa-chevron-down"></span>
									</a>
									<ul className="nav child_menu">
										<li>
											<a href="#">My Progress</a>
										</li>
									</ul>
								</li>
								<li>
									<a>
										<i className="fa fa-table"></i> Certificates 
										<span className="fa fa-chevron-down"></span>
									</a>
									<ul className="nav child_menu">
										<li>
											<a href="#">My Certificates</a>
										</li>
									</ul>
								</li>
							</ul>
						</div>
					</div>
					<div className="sidebar-footer hidden-small">
					  <a data-toggle="tooltip" data-placement="top" title="Settings">
						<i className="fa fa-gear" ></i>
					  </a>
					  <a data-toggle="tooltip" data-placement="top" title="FullScreen">
						<i className="fa fa-expand" ></i>
					  </a>
					  <a data-toggle="tooltip" data-placement="top" title="Lock">
						<i className="fa fa-eye-slash" ></i>
					  </a>
					  <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
						<i className="fa fa-power-off" ></i>
					  </a>
					</div>
				</div>
			
			</div>
		
		</div>
				<div className="top_nav">
          <div className="nav_menu">
            <nav>
              <div className="nav toggle">
                <a id="menu_toggle"><i className="fa fa-bars"></i></a>
              </div>

              <ul className="nav navbar-nav navbar-right">
                <li className="">
                  <a href="javascript:;" className="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="/img/img.jpg" alt="" />Walter Sobchak
                    <span className="fa fa-angle-down"></span>
                  </a>
                  <ul className="dropdown-menu dropdown-usermenu pull-right">
                    <li>
                      <a href="javascript:;">
                        <span className="badge bg-red pull-right">50%</span>
                        <span>Profile</span>
                      </a>
                    </li>
                    <li><a href="javascript:;">FAQ's</a></li>
                    <li><a href="login.html"><i className="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>

                <li role="presentation" className="dropdown">
                  <a href="javascript:;" className="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                    <i className="fa fa-envelope-o"></i>
                    <span className="badge bg-green">6</span>
                  </a>
                  <ul id="menu1" className="dropdown-menu list-unstyled msg_list" role="menu">
                    <li>
                      <a>
                        <span className="image"><img src="/img/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span className="time">3 mins ago</span>
                        </span>
                        <span className="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span className="image"><img src="/img/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span className="time">3 mins ago</span>
                        </span>
                        <span className="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span className="image"><img src="/img/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span className="time">3 mins ago</span>
                        </span>
                        <span className="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span className="image"><img src="/img/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span className="time">3 mins ago</span>
                        </span>
                        <span className="message">
                          Film festivals used to be movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <div className="text-center">
                        <a>
                          <strong>See All Alerts</strong>
                          <i className="fa fa-angle-right"></i>
                        </a>
                      </div>
                    </li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
	</div>
	
);


export default Dashboardheader;
