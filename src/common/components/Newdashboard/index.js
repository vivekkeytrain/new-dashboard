import React, { Component } from "react";
import { connect } from "react-redux";

import Dashboardfooter from "src/common/components/Dashboard/dashboardfooter";
import Dashboardheader from "src/common/components/Dashboard/dashboardheader";

import { fetchStudentDashboardHome } from "src/common/actions";

//import style from "./profile.scss";

class NewDashboard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      userData: null
    };
    this.renderChart = this.renderHorizontalChart.bind(this);
  }
  renderHorizontalChart(chartData) {
    var categories = [];
    var values = [];
    for (var i = 0; i < chartData.length; i++) {
      categories.push(chartData[i].course_abbrev);
      values.push(chartData[i].current_level);
    }
    Highcharts.chart("hor_bar_container", {
      chart: {
        type: "bar"
      },
      title: {
        text: ""
      },
      subtitle: {
        text: ""
      },
      xAxis: {
        categories: categories,
        title: {
          text: null
        }
      },
      yAxis: {
        min: 0,
        max: 7,
        title: {
          text: "Levels",
          align: "high"
        },
        labels: {
          overflow: "justify"
        }
      },
      tooltip: {
        valueSuffix: " level"
      },
      plotOptions: {
        bar: {
          dataLabels: {
            enabled: true
          }
        }
      },
      legend: {
        layout: "vertical",
        align: "right",
        verticalAlign: "top",
        x: -40,
        y: 80,
        floating: true,
        borderWidth: 1,
        backgroundColor: (Highcharts.theme &&
          Highcharts.theme.legendBackgroundColor) ||
          "#FFFFFF",
        shadow: true
      },
      credits: {
        enabled: false
      },
      series: [
        {
          name: null,
          data: values
        }
      ]
    });
  }

  componentDidMount() {
    console.log("newdashboard componentDidMount");
    this.props.onFetchStudentDashboardHome("S203").then(() => {
      console.log("fetched student dashboard home", this.props.user_data);

      this.setState({ userData: this.props.user_data });
      this.renderHorizontalChart(this.props.user_data);
    });
    // fetch(
    //   "http://sdapi.actkeytrain.com/dbwebapi/dbo.api_sp_get_student_dashboard_home/json?entity_tag=S203"
    // )
    //   .then(result => result.json())
    //   .then(result => {
    //     this.setState({ userData: result.ResultSets[0] });
    //     this.renderHorizontalChart(this.state.userData);
    //   });
  }

  render() {
    if (this.state.userData != null) {
      console.log("got data2-" + this.state.userData);
    }

    return (
      <div>
        <div className="container body">
          <Dashboardheader />
          <div className="right_col" role="main">
            <div className="row">
              <div className="col-md-12 col-sm-12 col-xs-12">
                <div className="x_panel tile ">
                  <div className="x_title">
                    <h2>Level of Completion</h2>
                    <ul className="nav navbar-right panel_toolbox">
                      <li>
                        <a className="collapse-link">
                          <i className="fa fa-chevron-up" />
                        </a>
                      </li>
                      <li className="dropdown">
                        <a
                          href="#"
                          className="dropdown-toggle"
                          data-toggle="dropdown"
                          role="button"
                          aria-expanded="false"
                        >
                          <i className="fa fa-wrench" />
                        </a>
                        <ul className="dropdown-menu" role="menu">
                          <li>
                            <a href="#">Settings 1</a>
                          </li>
                          <li>
                            <a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li>
                        <a className="close-link">
                          <i className="fa fa-close" />
                        </a>
                      </li>
                    </ul>
                    <div className="clearfix" />
                  </div>
                  <div className="x_content">
                    <div className="widget_summary">
                      	<div className="newBar col-md-9 col-sm-12 col-xs-12" >
							<table className="col-md-12 col-sm-12 col-xs-12">
								<tbody>
									<tr className="graph-data-row" >
										<td className="graph-key" width="" >Applied Mathematics</td>
										<td className="graph-val data-exampt" title="Exampt" width="11%" ><div></div></td>
										<td className="graph-val data-exampt" title="Exampt" width="11%" ><div></div></td>
										<td className="graph-val data-exampt" title="Exampt" width="11%" ><div></div></td>
										<td className="graph-val data-passed" title="Passed" width="11%" ><div></div></td>
										<td className="graph-val data-passed" title="Passed" width="11%" ><div></div></td>
										<td className="graph-val data-ready-next-step" title="Ready/Next Step" width="11%" ><div></div></td>
										<td className="graph-val" width="11%" ><div></div></td>
									</tr>
									<tr className="graph-data-row" >
										<td className="graph-key" >Reading for Information</td>
										<td className="graph-val data-exampt" title="Exampt" ><div></div></td>
										<td className="graph-val data-exampt" title="Exampt" ><div></div></td>
										<td className="graph-val data-passed" title="Passed"  ><div></div></td>
										<td className="graph-val data-passed" title="Passed"  ><div></div></td>
										<td className="graph-val data-working-in-progress" title="Working/In Progress"  ><div></div></td>
										<td className="graph-val data-goal" title="Goal"  ><div></div></td>
										<td className="graph-val" ><div></div></td>
									</tr>
									<tr className="graph-data-row" >
										<td className="graph-key" >Locating Information</td>
										<td className="graph-val graph-active data-exampt" title="Exampt" ><div></div></td>
										<td className="graph-val graph-active data-exampt" title="Exampt" ><div></div></td>
										<td className="graph-val data-passed" title="Passed" ><div></div></td>
										<td className="graph-val data-ready-next-step" title="Ready/Next Step" ><div></div></td>
										<td className="graph-val data-goal" title="Goal" ><div></div></td>
										<td className="graph-val data-goal" title="Goal" ><div></div></td>
										<td className="graph-val data-doesnt-exit" title="Doesn't Exist" ><div></div></td>
									</tr>
									<tr className="graph-lebel-row" >
										<td className="text-right" ></td>
										<td className="text-center" >1</td>
										<td className="text-center" >2</td>
										<td className="text-center" >3</td>
										<td className="text-center" >4</td>
										<td className="text-center" >5</td>
										<td className="text-center" >6</td>
										<td className="text-center" >7</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div className="graph-lebel-contener col-md-3" >
							<table className="col-md-12">
								<tbody>
									<tr className="graph-data-row" >
										<td className="graph-val data-exampt"><div></div></td>
										<td className="graph-data-lebel" width="60%" >Exampt</td>
									</tr>
									<tr className="graph-data-row" >
										<td className="graph-val data-passed" ><div></div></td>
										<td className="graph-data-lebel" >Passed</td>
									</tr>
									<tr className="graph-data-row" >
										<td className="graph-val data-goal" ><div></div></td>
										<td className="graph-data-lebel" >Goal</td>
									</tr>
									<tr className="graph-data-row" >
										<td className="graph-val data-doesnt-exit" ><div></div></td>
										<td className="graph-data-lebel" >Doesn't Exist</td>
									</tr>
									<tr className="graph-data-row" >
										<td className="graph-val data-working-in-progress" ><div></div></td>
										<td className="graph-data-lebel" >Working/In Progress</td>
									</tr>
									<tr className="graph-data-row" >
										<td className="graph-val data-ready-next-step" ><div></div></td>
										<td className="graph-data-lebel" >Ready/Next Step</td>
									</tr>
									<tr className="graph-data-row" >
										<td className="graph-val data-optional" ><div></div></td>
										<td className="graph-data-lebel" >Optional</td>
									</tr>
								</tbody>
							</table>
						</div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
            <br />
          </div>
        </div>

      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    user_data: state.root.user_data
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    onFetchStudentDashboardHome: entity_tag => {
      return dispatch(fetchStudentDashboardHome(entity_tag));
    }
  };
};

const ConnectedApp = connect(mapStateToProps, mapDispatchToProps)(NewDashboard);

export default ConnectedApp;
