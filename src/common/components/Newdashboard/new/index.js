import React, { Component } from "react";
import { connect } from "react-redux";

import Dashboardfooter from "src/common/components/Dashboard/dashboardfooter";
import Dashboardheader from "src/common/components/Dashboard/dashboardheader";

//import style from "./profile.scss";

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      userData: []
    };
	this.renderChart = this.renderHorizontalChart.bind(this);
  };
  renderHorizontalChart(chartData) {
	
  };
  
  componentDidMount() {
    console.log("newdashboard componentDidMount");
    fetch(
      "http://sdapi.actkeytrain.com/dbwebapi/dbo.api_sp_get_student_dashboard_home/json?entity_tag=S203"
    )
	.then(result => result.json())
	.then(result=> {
		this.setState({userData:result.ResultSets[0]});
		this.renderHorizontalChart(this.state.userData);
	});
  }

  render() {
	if(this.state.userData != null) {
		console.log("got data2-"+this.state.userData);
	}
	
    return (
      <div>
        <div className="container body">
          <Dashboardheader />
          <div className="right_col" role="main">
            <div className="row">
              <div className="col-md-12 col-sm-12 col-xs-12">
                <div className="x_panel tile ">
                  <div className="x_title">
                    <h2>Level of Completion</h2>
                    <ul className="nav navbar-right panel_toolbox">
                      <li>
                        <a className="collapse-link">
                          <i className="fa fa-chevron-up" />
                        </a>
                      </li>
                      <li className="dropdown">
                        <a
                          href="#"
                          className="dropdown-toggle"
                          data-toggle="dropdown"
                          role="button"
                          aria-expanded="false"
                        >
                          <i className="fa fa-wrench" />
                        </a>
                        <ul className="dropdown-menu" role="menu">
                          <li>
                            <a href="#">Settings 1</a>
                          </li>
                          <li>
                            <a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li>
                        <a className="close-link">
                          <i className="fa fa-close" />
                        </a>
                      </li>
                    </ul>
                    <div className="clearfix" />
                  </div>
                  <div className="x_content">
                    <div className="widget_summary">
					<div className="newBar col-md-12 col-sm-12 col-xs-12" >
						<table className="col-md-12 col-sm-12 col-xs-12">
							<tbody>
								<tr >
									<td className="graph-key" >Keytrain Intro</td>
									<td className="graph-val graph-active" ><div></div></td>
									<td className="graph-val graph-active" ><div></div></td>
									<td className="graph-val graph-active" ><div></div></td>
									<td className="graph-val" ></td>
									<td className="graph-val" ></td>
									<td className="graph-val" ></td>
									<td className="graph-val" ></td>
								</tr>
								<tr className="graph-data-row" >
									<td className="graph-key" >Reading Info</td>
									<td className="graph-val graph-active" ><div></div></td>
									<td className="graph-val graph-active" ><div></div></td>
									<td className="graph-val graph-active" ><div></div></td>
									<td className="graph-val graph-active" ><div></div></td>
									<td className="graph-val graph-active" ><div></div></td>
									<td className="graph-val graph-active" ><div></div></td>
									<td className="graph-val" ></td>
								</tr>
								<tr className="graph-data-row" >
									<td className="graph-key" >Applied Math</td>
									<td className="graph-val graph-active" ><div></div></td>
									<td className="graph-val graph-active" ><div></div></td>
									<td className="graph-val graph-active" ><div></div></td>
									<td className="graph-val graph-active" ><div></div></td>
									<td className="graph-val graph-active" ><div></div></td>
									<td className="graph-val" ></td>
									<td className="graph-val" ></td>
								</tr>
								<tr className="graph-data-row" >
									<td className="graph-key" >Locating Info</td>
									<td className="graph-val graph-active" ><div></div></td>
									<td className="graph-val graph-active" ><div></div></td>
									<td className="graph-val" ></td>
									<td className="graph-val" ></td>
									<td className="graph-val" ></td>
									<td className="graph-val" ></td>
									<td className="graph-val" ></td>
								</tr>
								<tr className="graph-lebel-row" >
									<td className="text-right" >0</td>
									<td className="text-right" >1</td>
									<td className="text-right" >2</td>
									<td className="text-right" >3</td>
									<td className="text-right" >4</td>
									<td className="text-right" >5</td>
									<td className="text-right" >6</td>
									<td className="text-right" >7</td>
								</tr>
							</tbody>
						</table>
					</div>
					</div>
                  </div>
                </div>
              </div>
            </div>
            <br />
          </div>
        </div>

      </div>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {};
};

const ConnectedApp = connect(mapStateToProps, mapDispatchToProps)(App);

export default ConnectedApp;