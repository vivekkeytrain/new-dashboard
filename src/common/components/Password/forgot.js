import React, { Component, PropTypes } from "react";
import { Link } from "react-router";

class Forgot extends Component {
  constructor(props) {
    super(props);

    this.state = {
      submitted: false
    };

    this.onPasswordReset = this.onPasswordReset.bind(this);
  }

  componentDidMount() {
    this._username.focus();
  }

  onPasswordReset(e) {
    console.log("onPasswordReset");
    e.preventDefault();

    this.setState({ submitted: true });

    this.props.onPasswordReset();
  }

  render() {
    console.log("render forgot");
    return (
      <div className="form-box signin" id="login-box">
        <div
          id="forgot-box"
          className="widget-box visible"
          ref={d => this.forgotBox = d}
        >
          <div className="header bg-navy">
            {this.props.site_type == 2
              ? <img src="/img/ACT_CR101_logo_rev_lg.png" width="330" alt="" />
              : <img
                  src="/img/act-keytrain_rev.png"
                  alt=""
                  width="330"
                  className="img-responsive"
                />}
          </div>
          <div className="bg-gray" style={{ padding: "20px 10px" }}>
            <h4 className="red">Retrieve Your Password</h4>
          </div>
          <form onSubmit={this.onPasswordReset} role="form" noValidate>
            <div className="bg-gray body">
              <p>
                {this.state.submitted
                  ? "Please check your email."
                  : "Provide the your username and we will send you a password reset link."}
              </p>
              <div className="form-group">
                <label htmlFor="username" className="control-label">
                  Username
                </label>
                <div className="controls">
                  <input
                    type="text"
                    name="username"
                    id="username"
                    className="form-control"
                    value={this.props.username}
                    onChange={this.props.onUsernameChange}
                    readOnly={this.state.submitted}
                    ref={r => this._username = r}
                  />
                  <p className="help-block" />
                </div>
              </div>
            </div>

            <div className="footer">
              <button
                type="submit"
                id="forgot-submit-button"
                className="btn btn-primary btn-block btn-lg"
                disabled={this.state.submitted}
              >
                {this.state.submitted ? "Check your email" : "Submit"}
              </button>
              <div>
                <span className="for-signin inline">
                  <Link to="/">
                    Back to login
                  </Link>
                </span>
                <br />
              </div>
            </div>
          </form>
        </div>
        <br /><br />
      </div>
    );
  }
}

Forgot.propTypes = {
  site_type: PropTypes.number,
  username: PropTypes.string,
  onUsernameChange: PropTypes.func,
  onPasswordReset: PropTypes.func
};

export default Forgot;
