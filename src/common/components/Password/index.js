import React, { Component } from "react";
import { connect } from "react-redux";

import { sendPasswordReset, fetchUserData } from "src/common/actions";

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            username: "",
            password1: "",
            password2: ""
        };

        this.onUsernameChange = this.onUsernameChange.bind(this);
        this.onPassword1Change = this.onPassword1Change.bind(this);
        this.onPassword2Change = this.onPassword2Change.bind(this);

        this.onPasswordReset = this.onPasswordReset.bind(this);
        this.onChangePassword = this.onChangePassword.bind(this);
    }

    onUsernameChange(e) {
        this.setState({ username: e.target.value });
    }

    onPassword1Change(e) {
        this.setState({ password1: e.target.value });
    }

    onPassword2Change(e) {
        this.setState({ password2: e.target.value });
    }

    onPasswordReset() {
        this.props.onPasswordReset(this.state.username).then(() => {
            console.log("done sendPasswordReset", this.props.status);

            if (this.props.status == 0) {
            }
        });
    }

    onChangePassword(e) {
        e.preventDefault();

        this.props.onChangePassword();
    }

    render() {
        console.log("render password index", this.state.username);
        const children_with_props = React.Children.map(
            this.props.children,
            child => React.cloneElement(child, {
                site_type: this.props.site_type,
                user_data: this.props.user_data,
                username: this.state.username,
                password1: this.state.password1,
                password2: this.state.password2,
                onUsernameChange: this.onUsernameChange,
                onPassword1Change: this.onPassword1Change,
                onPassword2Change: this.onPassword2Change,
                onPasswordReset: this.onPasswordReset,
                onChangePassword: this.onChangePassword,
                fetchUserData: this.props.fetchUserData
            })
        );

        // console.log("children_with_props");
        // console.log(children_with_props);

        return (
            <div>
                {children_with_props}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        site_type: state.root.site_type,
        user_data: state.root.user_data,
        status: state.root.status
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        onPasswordReset: username => {
            return dispatch(sendPasswordReset(username));
        },
        fetchUserData: luid => {
            return dispatch(fetchUserData(luid));
        }
    };
};

const ConnectedApp = connect(mapStateToProps, mapDispatchToProps)(App);

export default ConnectedApp;
