import React, { PropTypes } from "react";

import zxcvbn from "zxcvbn";

const PasswordMeter = ({ password }) => {
  console.log("render PasswordMeter", password);
  console.log(zxcvbn);
  if (!password) return <span />;

  let strength = {
    0: "Worst",
    1: "Bad",
    2: "Weak",
    3: "Good",
    4: "Strong"
  };

  let result;
  if (password.length < 8) {
    result = {
      score: 0,
      feedback: {
        warning: "This password is not long enough. At least 8 characters are required.",
        suggestions: ""
      }
    };
  } else {
    result = zxcvbn(password);
  }

  return (
    <span>
      <meter max="4" value={result.score} />
      <p>
        Strength:{" "}
        <strong>{strength[result.score]}</strong>
        {String.fromCharCode(160) /*&nbsp;*/}
        <span className="feedback">
          {result.feedback.warning}
          {String.fromCharCode(160) /*&nbsp;*/}
          {result.feedback.suggestions}
        </span>
      </p>
    </span>
  );
};

PasswordMeter.propTypes = {
  password: PropTypes.string
};

export default PasswordMeter;
