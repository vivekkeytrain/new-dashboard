import React, { Component, PropTypes } from "react";
import { Link } from "react-router";

import PasswordMeter from "./password_meter";
import Notices from "src/common/components/Login/notices";

//require("./reset.scss");

class Reset extends Component {
  constructor(props) {
    // console.log("Reset constructor", props);
    super(props);

    this.state = {
      password: "",
      notices: []
    };

    this.onChangePassword = this.onChangePassword.bind(this);
    this.onPasswordChange = this.onPasswordChange.bind(this);
  }

  componentDidMount() {
    this.props.fetchUserData(this.props.params.uid);
    this._password1.focus();
  }

  onChangePassword(e) {
    console.log("onChangePassword", this._password1.value, this._password2);
    e.preventDefault();

    if (this._password1.value != this._password2.value) {
      this.setState({
        notices: [
          {
            type: "warning",
            message: "Passwords do not match"
          }
        ]
      });
      return;
    }

    this.props.onChangePassword();
  }

  onPasswordChange(e) {
    let password = e.target.value;

    this.setState({
      password
    });
  }

  render() {
    console.log("render reset");
    // console.log(this.props);
    return (
      <div className="form-box signin" id="login-box">
        <div className="widget-box visible">
          <div className="header bg-navy">
            {this.props.site_type == 2
              ? <img src="/img/ACT_CR101_logo_rev_lg.png" width="330" alt="" />
              : <img
                  src="/img/act-keytrain_rev.png"
                  alt=""
                  width="330"
                  className="img-responsive"
                />}
          </div>
          <div className="bg-gray" style={{ padding: "10px 10px 0" }}>
            <h4 className="red">Reset Your Password</h4>
          </div>
          <form
            onSubmit={this.onChangePassword}
            role="form"
            noValidate
            className="reset-form"
          >
            <div className="bg-gray body">
              <Notices notices={this.state.notices} />
              <div className="controls">
                <label htmlFor="password1" className="control-label">
                  New Password
                </label>
                <input
                  type="password"
                  name="password1"
                  id="password1"
                  className="form-control"
                  value={this.state.password}
                  onChange={this.onPasswordChange}
                  ref={r => this._password1 = r}
                />
                <PasswordMeter password={this.state.password} />
                <label htmlFor="password2" className="control-label">
                  Verify Password
                </label>
                <input
                  type="password"
                  name="password2"
                  id="password2"
                  className="form-control"
                  ref={r => this._password2 = r}
                />
              </div>
            </div>

            <div className="footer">
              <button
                type="s4ubmit"
                id="forgot-submit-button"
                className="btn btn-primary btn-block btn-lg"
                disabled={this.state.password.length < 8}
              >
                Submit
              </button>
              <div>
                <span className="for-signin inline">
                  <Link to="/">
                    Back to login
                  </Link>
                </span>
                <br />
              </div>
            </div>
          </form>
        </div>
        <br /><br />
      </div>
    );
  }
}

Reset.propTypes = {
  site_type: PropTypes.number,
  fetchUserData: PropTypes.func
};

export default Reset;
