import React, { Component } from "react";
import { connect } from "react-redux";

import Dashboardfooter from "src/common/components/Dashboard/dashboardfooter";
import Dashboardheader from "src/common/components/Dashboard/dashboardheader";

//require("./profile.scss");
//import style from "../Dashboard/profile.scss";

class App extends Component {
    render() {
        console.log("render profile index");

        return (
            <div>
                <Dashboardheader />
                <div id="profile-contener">
                    <div className="row pad-tb-s70 brand-white-bg gray-dark">
                        <div className="container">
                            <div className="row main">
                                <div className="col-md-12">
                                    <div className="main-login main-center">
                                        <h2>
                                            Update User Information & Organization Access
                                        </h2>
                                        <form
                                            className=""
                                            method="post"
                                            action="#"
                                        >
                                            <div className="row">
                                                <div
                                                    className="col-md-8 form-user-left"
                                                >
                                                    <h4>User Information</h4>
                                                    <div className="form-group">
                                                        <label
                                                            htmlFor="s_name"
                                                            className="cols-sm-2 control-label"
                                                        >
                                                            School Name
                                                        </label>
                                                        <div
                                                            className="cols-sm-10"
                                                        >
                                                            <div
                                                                className="input-group"
                                                            >
                                                                <span
                                                                    className="input-group-addon"
                                                                >
                                                                    <i
                                                                        className="fa fa-institution fa"
                                                                        aria-hidden="true"
                                                                    />
                                                                </span>
                                                                <input
                                                                    type="text"
                                                                    className="form-control"
                                                                    name="s_name"
                                                                    id="name"
                                                                    placeholder="Vivek Demo"
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="form-group">
                                                        <label
                                                            htmlFor="l_name"
                                                            className="cols-sm-2 control-label"
                                                        >
                                                            Full Name{" "}
                                                            <span
                                                                className="required"
                                                            >
                                                                *
                                                            </span>
                                                        </label>
                                                        <div
                                                            className="cols-sm-10"
                                                        >
                                                            <div
                                                                className="input-group"
                                                            >
                                                                <span
                                                                    className="input-group-addon"
                                                                >
                                                                    <i
                                                                        className="fa fa-user fa"
                                                                        aria-hidden="true"
                                                                    />
                                                                </span>
                                                                <input
                                                                    type="text"
                                                                    className="form-control"
                                                                    name="l_name"
                                                                    id="l_name"
                                                                    placeholder="Last Name"
                                                                />
                                                                <input
                                                                    type="text"
                                                                    className="form-control"
                                                                    name="m_name"
                                                                    id="m_name"
                                                                    placeholder="Middle Name"
                                                                />
                                                                <input
                                                                    type="text"
                                                                    className="form-control"
                                                                    name="f_name"
                                                                    id="f_name"
                                                                    placeholder="First Name"
																	required="true"
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="form-group">
                                                        <label
                                                            htmlFor="user_name"
                                                            className="cols-sm-2 control-label"
                                                        >
                                                            User Name{" "}
                                                            <span
                                                                className="required"
                                                            >
                                                                *
                                                            </span>
                                                        </label>
                                                        <div
                                                            className="cols-sm-10"
                                                        >
                                                            <div
                                                                className="input-group"
                                                            >
                                                                <span
                                                                    className="input-group-addon"
                                                                >
                                                                    <i
                                                                        className="fa fa-address-card-o fa"
                                                                        aria-hidden="true"
                                                                    />
                                                                </span>
                                                                <input
                                                                    type="text"
                                                                    className="form-control"
                                                                    name="user_name"
                                                                    id="user_name"
                                                                    placeholder="vivek-admin"
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="form-group">
                                                        <label
                                                            htmlFor="pass"
                                                            className="cols-sm-2 control-label"
                                                        >
                                                            Password{" "}
                                                            <span
                                                                className="required"
                                                            >
                                                                *
                                                            </span>
                                                        </label>
                                                        <div
                                                            className="cols-sm-10"
                                                        >
                                                            <div
                                                                className="input-group"
                                                            >
                                                                <span
                                                                    className="input-group-addon"
                                                                >
                                                                    <i
                                                                        className="fa fa-lock fa"
                                                                        aria-hidden="true"
                                                                    />
                                                                </span>
                                                                <input
                                                                    type="password"
                                                                    className="form-control"
                                                                    name="password"
                                                                    id="password"
                                                                    placeholder="Password"
                                                                />
                                                                <input
                                                                    type="password"
                                                                    className="form-control"
                                                                    name="varify_password"
                                                                    id="varify_password"
                                                                    placeholder="Varify Password"
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="form-group">
                                                        <label
                                                            htmlFor="name"
                                                            className="cols-sm-2 control-label"
                                                        >
                                                            Phone/Email{" "}
                                                            <span
                                                                className="required"
                                                            >
                                                                *
                                                            </span>
                                                        </label>
                                                        <div
                                                            className="cols-sm-10"
                                                        >
                                                            <div
                                                                className="input-group"
                                                            >
                                                                <span
                                                                    className="input-group-addon"
                                                                >
                                                                    <i
                                                                        className="fa fa-phone fa"
                                                                        aria-hidden="true"
                                                                    />
                                                                </span>
                                                                <input
                                                                    type="text"
                                                                    className="form-control"
                                                                    name="phone"
                                                                    id="phone"
                                                                    placeholder="+1 123 456 7896"
                                                                />
                                                                <input
                                                                    type="text"
                                                                    className="form-control"
                                                                    name="email"
                                                                    id="email"
																	required={true}
																	minCharacters={6}
																	validate={this.validateEmail}
																	errorMessage="Email is invalid"
																	emptyMessage="Email is required"
                                                                    placeholder="Vivek.Munukuntla@act.org"
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div
                                                        className="form-group float-md-left1"
                                                    >
                                                        <label
                                                            htmlFor="address"
                                                            className="cols-sm-2 control-label"
                                                        >
                                                            Address 1
                                                        </label>
                                                        <div
                                                            className="cols-sm-10"
                                                        >
                                                            <div
                                                                className="input-group"
                                                            >
                                                                <span
                                                                    className="input-group-addon"
                                                                >
                                                                    <i
                                                                        className="fa fa-map-marker fa"
                                                                        aria-hidden="true"
                                                                    />
                                                                </span>
                                                                <textarea
                                                                    rows="5"
                                                                    cols="39"
                                                                    id="address"
                                                                    placeholder="Address 1"
                                                                />
                                                                <textarea
                                                                    rows="5"
                                                                    cols="39"
                                                                    placeholder="Address 2"
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="form-group">
                                                        <label
                                                            htmlFor="city"
                                                            className="cols-sm-2 control-label"
                                                        >
                                                            City/State/Zipcode
                                                        </label>
                                                        <div
                                                            className="cols-sm-10"
                                                        >
                                                            <div
                                                                className="input-group"
                                                            >
                                                                <span
                                                                    className="input-group-addon"
                                                                >
                                                                    <i
                                                                        className="fa fa-map fa"
                                                                        aria-hidden="true"
                                                                    />
                                                                </span>
                                                                <input
                                                                    type="text"
                                                                    className="form-control"
                                                                    name="city"
                                                                    id="city"
                                                                    placeholder="City"
                                                                />
                                                                <input
                                                                    type="text"
                                                                    className="form-control"
                                                                    name="state"
                                                                    id="state"
                                                                    placeholder="State"
                                                                />
                                                                <input
                                                                    type="text"
                                                                    className="form-control"
                                                                    name="zipcode"
                                                                    id="zipcode"
                                                                    placeholder="Zipcode"
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div className="form-group">
                                                        <label
                                                            htmlFor="status"
                                                            className="cols-sm-2 control-label"
                                                        >
                                                            Status
                                                        </label>
                                                        <div
                                                            className="cols-sm-10"
                                                        >
                                                            <div
                                                                className="input-group status"
                                                            >
                                                                <span
                                                                    className="input-group-addon"
                                                                >
                                                                    <i
                                                                        className="fa-calendar-check-o fa"
                                                                        aria-hidden="true"
                                                                    />
                                                                </span>
                                                                <div
                                                                    className="col-md-2"
                                                                >
                                                                    <label>
                                                                        <input
                                                                            type="checkbox"
                                                                            className="form-control"
                                                                            name="status"
                                                                            id="status"
                                                                        />
                                                                        {" "}
                                                                        Active
                                                                    </label>
                                                                </div>
                                                                <div
                                                                    className="col-md-3"
                                                                >
                                                                    <label>
                                                                        <input
                                                                            type="radio"
                                                                            className="form-control"
                                                                            name="radio"
                                                                            id="admin1"
                                                                        />
                                                                        {" "}
                                                                        Administrator
                                                                    </label>
                                                                </div>
                                                                <div
                                                                    className="col-md-4"
                                                                >
                                                                    <label>
                                                                        <input
                                                                            type="radio"
                                                                            className="form-control"
                                                                            name="radio"
                                                                            id="admin2"
                                                                        />
                                                                        {" "}
                                                                        Administrator (Reports only)
                                                                    </label>
                                                                </div>
                                                                <div
                                                                    className="col-md-3"
                                                                >
                                                                    <label>
                                                                        <input
                                                                            type="radio"
                                                                            className="form-control"
                                                                            name="radio"
                                                                            id="admin3"
                                                                        />
                                                                        {" "}
                                                                        Instructor
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="form-group">
                                                        <label
                                                            htmlFor="name"
                                                            className="cols-sm-2 control-label"
                                                        >
                                                            Notes
                                                        </label>
                                                        <div
                                                            className="cols-sm-10"
                                                        >
                                                            <div
                                                                className="input-group"
                                                            >
                                                                <span
                                                                    className="input-group-addon"
                                                                >
                                                                    <i
                                                                        className="fa-file-text fa"
                                                                        aria-hidden="true"
                                                                    />
                                                                </span>
                                                                <textarea
                                                                    rows="5"
                                                                    cols="39"
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div
                                                    className="col-md-4 form-user-right"
                                                >
                                                    <h4>Organization Access</h4>
                                                    <p>
                                                        This user has access to the 13 organizations below.


                                                        {" "}
                                                        <a href="#">Manage</a>
                                                    </p>
                                                    <div className="">
                                                        <ul>
                                                            <li>
                                                                <input
                                                                    type="checkbox"
                                                                    name=""
                                                                />
                                                                {" "}
                                                                <strong>
                                                                    Vivek Demo (default)
                                                                </strong>
                                                                {" "}
                                                            </li>
                                                            <li>
                                                                <input
                                                                    type="checkbox"
                                                                    name=""
                                                                />
                                                                {" "}
                                                                ACT Career Curriculum


                                                                {" "}
                                                            </li>
                                                            <li>
                                                                <input
                                                                    type="checkbox"
                                                                    name=""
                                                                />
                                                                {" "}
                                                                ACT Curriculum Review


                                                                {" "}
                                                            </li>
                                                            <li>
                                                                <input
                                                                    type="checkbox"
                                                                    name=""
                                                                />
                                                                {" "}
                                                                ACT Curriculum Team


                                                                {" "}
                                                            </li>
                                                            <li>
                                                                <input
                                                                    type="checkbox"
                                                                    name=""
                                                                />
                                                                {" "}
                                                                ACT Demo Accounts


                                                                {" "}
                                                            </li>
                                                            <li>
                                                                <input
                                                                    type="checkbox"
                                                                    name=""
                                                                />
                                                                {" "}
                                                                ACT Employees Demo


                                                                {" "}
                                                            </li>
                                                            <li>
                                                                <input
                                                                    type="checkbox"
                                                                    name=""
                                                                />
                                                                {" "}
                                                                Anand Demo
                                                                {" "}
                                                            </li>
                                                            <li>
                                                                <input
                                                                    type="checkbox"
                                                                    name=""
                                                                />
                                                                {" "}Coptix{" "}
                                                            </li>
                                                            <li>
                                                                <input
                                                                    type="checkbox"
                                                                    name=""
                                                                />
                                                                {" "}
                                                                Coptix - Limited
                                                                {" "}
                                                            </li>
                                                            <li>
                                                                <input
                                                                    type="checkbox"
                                                                    name=""
                                                                />
                                                                {" "}
                                                                HTML5 Mobile Preview


                                                                {" "}
                                                            </li>
                                                            <li>
                                                                <input
                                                                    type="checkbox"
                                                                    name=""
                                                                />
                                                                {" "}
                                                                KeyTrain Individual Users I-Store


                                                                {" "}
                                                            </li>
                                                            <li>
                                                                <input
                                                                    type="checkbox"
                                                                    name=""
                                                                />
                                                                {" "}
                                                                Little Lebowski Urban Achievers


                                                                {" "}
                                                            </li>
                                                            <li>
                                                                <input
                                                                    type="checkbox"
                                                                    name=""
                                                                />
                                                                {" "}
                                                                Total Quality Management


                                                                {" "}
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-md-2">
                                                    <a
                                                        href="#"
                                                        target="_blank"
                                                        type="button"
                                                        id="button"
                                                        className="btn btn-primary btn-lg btn-block login-button"
                                                    >
                                                        Save
                                                    </a>
                                                </div>
                                                <div className="col-md-3">
                                                    <a
                                                        href="#"
                                                        target="_blank"
                                                        type="button"
                                                        id="button"
                                                        className="btn btn-primary btn-lg btn-block login-button"
                                                    >
                                                        Save & New
                                                    </a>
                                                </div>
                                                <div className="col-md-3">
                                                    <a
                                                        href="#"
                                                        target="_blank"
                                                        type="button"
                                                        id="button"
                                                        className="btn btn-primary btn-lg btn-block login-button"
                                                    >
                                                        Save & Close
                                                    </a>
                                                </div>
                                                <div className="col-md-2">
                                                    <a
                                                        href="#"
                                                        target="_blank"
                                                        type="button"
                                                        id="button"
                                                        className="btn btn-primary btn-lg btn-block login-button"
                                                    >
                                                        Reset
                                                    </a>
                                                </div>
                                                <div className="col-md-2">
                                                    <a
                                                        href="#"
                                                        target="_blank"
                                                        type="button"
                                                        id="button"
                                                        className="btn btn-primary btn-lg btn-block login-button"
                                                    >
                                                        Cancel
                                                    </a>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <Dashboardfooter />
            </div>
        );
    }
	
}

const validateEmail = function (value) {
	// regex from http://stackoverflow.com/questions/46155/validate-email-address-in-javascript
	var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(value);
}
  
const mapStateToProps = state => {
    return {
        site_type: state.root.site_type,
        user_data: state.root.user_data,
        status: state.root.status
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {};
};

const ConnectedApp = connect(mapStateToProps, mapDispatchToProps)(App);

export default ConnectedApp;
