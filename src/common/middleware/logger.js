export const logger = store => next => action => {
  console.log("dispatching", action);
  let result = next(action);
  let state = store.getState();
  //    If we put this in the console.log it doesn't get stripped by webpack
  console.log("next state", state);
  return result;
};
