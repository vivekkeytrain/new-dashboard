export const isProduction = env =>
    (env || process.env).NODE_ENV === "production";
export const emailHost = env => (env || process.env).EMAIL_HOST;
export const emailPort = env => (env || process.env).EMAIL_PORT;
