import { combineReducers } from "redux";
import {
  FETCH_SIGNIN_REQUEST,
  FETCH_SIGNIN_FAILURE,
  FETCH_SIGNIN_SUCCESS,
  FETCH_SIGNUP_REQUEST,
  FETCH_SIGNUP_FAILURE,
  FETCH_SIGNUP_SUCCESS,
  FETCH_PASSWORD_RESET_REQUEST,
  FETCH_PASSWORD_RESET_FAILURE,
  FETCH_PASSWORD_RESET_SUCCESS,
  FETCH_STUDENT_DASHBOARD_HOME_REQUEST,
  FETCH_STUDENT_DASHBOARD_HOME_FAILURE,
  FETCH_STUDENT_DASHBOARD_HOME_SUCCESS
} from "src/common/actions";

function root(
  state = {
    fetching: false,
    notices: [],
    messages: [],
    user_data: {},
    site_type: 1,
    self_signup: null,
    status: 0
  },
  action
) {
  console.log("root", action, state);
  switch (action.type) {
    case FETCH_SIGNIN_REQUEST:
    case FETCH_SIGNUP_REQUEST:
    case FETCH_PASSWORD_RESET_REQUEST:
    case FETCH_STUDENT_DASHBOARD_HOME_REQUEST:
      return Object.assign({}, state, { fetching: true });

    case FETCH_SIGNIN_FAILURE:
    case FETCH_SIGNUP_FAILURE:
    case FETCH_PASSWORD_RESET_FAILURE:
    case FETCH_STUDENT_DASHBOARD_HOME_FAILURE:
      return Object.assign({}, state, {
        notices: [
          ...state.notices,
          { type: "warning", message: action.ex.message }
        ],
        fetching: false
      });

    case FETCH_SIGNIN_SUCCESS:
      return Object.assign({}, state, {
        user_data: action.user_data,
        fetching: false,
        notices: [{ type: "warning", message: action.user_data.statdesc }],
        messages: action.messages
      });

    case FETCH_PASSWORD_RESET_SUCCESS:
      return Object.assign({}, state, {
        feching: false,
        status: action.status
      });

    case FETCH_STUDENT_DASHBOARD_HOME_SUCCESS:
      return Object.assign({}, state, {
        user_data: action.user_data,
        fetching: false
      });

    default:
      return state;
  }
}

export default combineReducers({ root });
