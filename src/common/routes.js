import React from "react";
import { Route } from "react-router";

import Login from "src/common/components/Login";
import Layout from "src/common/components/Layout";
import Password from "src/common/components/Password";
import Forgot from "src/common/components/Password/Forgot";
import Reset from "src/common/components/Password/Reset";
import Profile from "src/common/components/Profile";
import Dashboard from "src/common/components/Dashboard";
import Newdashboard from "src/common/components/Newdashboard";

export default (
    <Route components={Layout}>
        <Route path="/" component={Login} />
        <Route path="/password" component={Password}>
            <Route path="forgot" component={Forgot} />
            <Route path="reset/:uid" component={Reset} />
        </Route>
        <Route path="profile" component={Profile} />
		<Route path="newdashboard" component={Newdashboard} />
    </Route>
);
