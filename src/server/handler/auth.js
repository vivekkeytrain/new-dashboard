import request from "request";
import url from "url";
import parseDomain from "parse-domain";

import redis from "redis";
import uuid from "node-uuid";
import email from "emailjs";
import mimelib from "mimelib";

import { login_domain } from "src/server/local_config";
import { emailHost, emailPort } from "src/common/pkg/env";

export default app => {
  app.use((req, res, next) => {
    //  Check to see if we're on nginx, otherwise load local config file
    //  Lacking X-server header is indication that we'er running locally.
    //  This header will be set in nginx congif

    if (!req.get("X-NginX-Proxy")) {
      req.login_domain = login_domain;
    } else {
      req.login_domain = req.get("HOST");
    }
    next();
  });

  app.use((req, res, next) => {
    /*  
      Check for existence of cookie 

      If cookie exists we call an api to check for validity.
      If still valid we redirect, if invalid delete cookie and continue to login.
    */
    // console.log('cookies', req.cookies);

    if (req.cookies.loginsessionuid) {
      console.log("found cookie", req.cookies.loginsessionuid);
      console.log(
        `http://sdapi.actkeytrain.com/dbwebapi/dbo.api_sp_login_session_check/json?luid_str=${req.cookies.loginsessionuid}`
      );

      let api_domain = req.login_domain.indexOf("keytrain") ? "snapi" : "sdapi";

      request(
        `http://${api_domain}.actkeytrain.com/dbwebapi/dbo.api_sp_login_session_check/json?luid_str=${req.cookies.loginsessionuid}`,
        (err, resp, body) => {
          console.log(body);
          let result = JSON.parse(body);

          if (
            !result.hasOwnProperty("ResultSets") ||
              result.ResultSets.length < 1 ||
              result.ResultSets[0].length < 1
          ) {
            //  Something wrong with the json, return an error
            res.status(500).send("API Error");
          }

          let resultset = result.ResultSets[0][0];
          //  Valid luid
          if (resultset.status == 0) {
            // console.log('redirect', `${resultset.site_url}${req.cookies.loginsessionuid}`);
            return res.redirect(
              `${resultset.site_url}${req.cookies.loginsessionuid}`
            );
          } else {
            //  Expire the cookie
            console.log("delete cookie");
            res.cookie("loginsessionuid", "", { expires: new Date() });
            next();
          }
        }
      );
    } else {
      next();
    }
  });

  app.post("/api/login", (req, res) => {
    console.log("/api/login");
    const { username, password } = req.body;

    // let u = url.parse(`http://${req.get("HOST")}`);
    // console.log(req.get("HOST"));
    // console.log(u);
    // console.log(u.hostname);
    // console.log(parseDomain(u.hostname));

    // http://sdapi.actkeytrain.com/dbwebapi/dbo.api_sp_login_working/json?userid=griselda&password=sdf&pkg_id=2&login_domain=dev.login.careerready101.com

    let api_domain = req.login_domain.indexOf("keytrain") ? "snapi" : "sdapi";
    let pkg_id = req.login_domain.indexOf("keytrain") !== -1 ? 1 : 2;

    console.log(
      `http://${api_domain}.actkeytrain.com/dbwebapi/dbo.api_sp_login_working/json?userid=${username}&password=${password}&pkg_id=${pkg_id}&login_domain=${req.login_domain}`
    );

    request(
      `http://${api_domain}.actkeytrain.com/dbwebapi/dbo.api_sp_login_working/?userid=${username}&password=${password}&pkg_id=${pkg_id}&login_domain=${req.login_domain}`,
      (err, resp, body) => {
        let result = JSON.parse(body);

        if (
          !result.hasOwnProperty("ResultSets") ||
            result.ResultSets.length < 1 ||
            result.ResultSets[0].length < 1
        ) {
          //  Something wrong with the json, return an error
          res.status(500).send("API Error");
        }

        //  If we've got a successful status we set the cookie
        if (result.ResultSets[0][0].status == 0) {
          // console.log("set cookie");

          let pd = parseDomain(u.hostname);
          console.log(pd);
          if (pd) {
            res.cookie(
              "loginsessionuid",
              result.ResultSets[0][0].login_session_uid,
              {
                domain: `.${pd.domain}.${pd.tld}`,
                expires: new Date(new Date().getTime() + 24 * 60 * 60 * 1000)
              }
            );
          }
        } else {
          //  Expire the cookie
          res.cookie("loginsessionuid", "", { expires: new Date() });
        }

        // console.log(result.ResultSets);
        res.json(result.ResultSets);
      }
    );
  });

  app.post("/api/password/send", (req, res) => {
    /*  
      Lookup user details 
      If we have a valid email address:
        * Generate uuid
        * Store for 30 minutes in redis with user details
        * Send email

      If we don't have a valid email
        * Return a message that they need to talk to their admin
    */

    const { username } = req.body;
    console.log("send", username);
    // console.log(req);

    let api_domain = req.login_domain.indexOf("keytrain") ? "snapi" : "sdapi";

    request(
      `http://${api_domain}.actkeytrain.com/dbwebapi/dbo.api_sp_get_user_details/json?username=${username}&modeflags=4`,
      (err, resp, body) => {
        if (err) {
          return res.status(500).json({
            status: 500,
            statdesc: "Error"
          });
        }
        let result = JSON.parse(body);

        if (result.ResultSets[0][0].status == 0) {
          let client = redis.createClient();

          client.on("error", function(err) {
            return res.status(500).json({
              status: 500,
              statdesc: err.message
            });
          });

          let uid = uuid.v4();
          // console.log(uid);

          const user_data = result.ResultSets[0][0];
          //  Save the data in redis for 30 minutes
          client.set(uid, JSON.stringify(user_data), "NX", "EX", 30 * 60, (
            err,
            rep
          ) => {
            let message = `Click the link below or paste it into your browser to reset your password.

${req.protocol}://${req.get("HOST")}/password/reset/${uid}

If you did not request a password reset you may simply ignore this email.
`;
            // console.log(message);

            // console.log("emailHost", emailHost());
            // console.log("emailPort", emailPort());

            // Once it's saved send the email
            const server = email.server.connect({
              host: emailHost(),
              port: emailPort()
            });
            // console.log(server);

            let subject = "Your password reset";

            const email_message = email.message.create({
              subject,
              text: message,
              from: `noreplay@${req.login_domain}`,
              to: user_data.email
            });
            // console.log(email_message);
            //  It's trying to mime-encode the subject it's coming out all wrong, so I'm just bypassing that
            //  There's probably a reason that the subject should be mime-encoded but the encoding seems to make it unreadable when you actually send it
            email_message.header.subject = subject;
            server.send(email_message, (err, mess) => console.log(err || mess));

            res.json({
              status: 0,
              statdesc: "Success"
            });
          });
        } else {
          return res.json({
            status: result.ResultSets[0][0].status,
            statdesc: result.ResultSets[0][0].statdesc
          });
        }
      }
    );
  });

  //  Get the data associated with uid for a password reset
  app.post("/api/password/data/", (req, res) => {
    /*
      Lookup the user data based upon uid, from redis
    */
    console.log("/api/password/data");
    console.log(req.body);
    const { uid } = req.body;
    console.log(`get ${uid} from redis`);

    let client = redis.createClient();

    client.on("error", function(err) {
      console.error(err);
    });

    client.get(uid, (err, rep) => {
      console.error(err);
      console.log(rep);
      if (err || !rep) {
        return res.json({
          status: 255,
          stat_desc: "User not found"
        });
      }

      return res.json(JSON.parse(rep));
    });
  });
};
