import express from "express";
import path from "path";
import React from "react";
import consolidate from "consolidate";
import { renderToString } from "react-dom/server";
import { Provider } from "react-redux";
import { match, RouterContext } from "react-router";
import bodyParser from "body-parser";
import cookieParser from "cookie-parser";
import request from "request";
// import flash from "connect-flash";
// import session from "express-session";

import handlers from "src/server/handler";
import configureStore from "src/common/store";
import routes from "src/common/routes";
// import { SESSION_SECRET, SESSION_EXPIRATION } from "src/common/config";

const port = process.env.PORT || 3000;
const app = express();
const root = path.join(process.cwd(), "public");

// export const isProduction = env =>
//   (env || process.env).NODE_ENV === "production";
import { isProduction } from "src/common/pkg/env";

if (!isProduction()) {
  const webpack = require("webpack");
  const webpackConfig = require("webpack.config.client");

  const compiler = webpack(webpackConfig);

  app.use(require("webpack-dev-middleware")(compiler, {
      noInfo: true,
      publicPath: webpackConfig.output.publicPath
    }));

  app.use(require("webpack-hot-middleware")(compiler));
}
app.use((req, res, next) => {
  next();
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(cookieParser());

// app.use(flash());

// var sess = {
//   secret: SESSION_SECRET,
//   cookie: { _expires: SESSION_EXPIRATION },
//   resave: false,
//   saveUninitialized: false
// };

// if (isProduction()) {
//   sess.cookie.secure = true; // serve secure cookies
// }

// app.use(session(sess));

app.use(express.static(root));
handlers.bindAll(app);

// app.get('/', (req, res, next) => {
// })
// app.post('/api/login', (req, res, next) => {
//     console.log(req);
//     res.send('done');
// })

let user_data = {};
app.get("/password/reset/:uid", (req, res, next) => {
  console.log("fetch data", req.params);
  request.post(
    `http://${req.get("HOST")}/api/password/data`,
    { form: { uid: req.params.uid } },
    (err, resp, body) => {
      // console.log("got data");
      user_data = JSON.parse(body);

      console.log("user_data status", user_data.status);
      if (user_data.status > 200) return res.redirect("/");
      next();
    }
  );
  console.log("here I am now");
});

// const self_signup = { orgzname: "Coptix" };
const self_signup = null;
app.use((req, res) => {
  // console.log("HOST", req.get("HOST"));
  console.log("login_domain", req.login_domain);
  const site_type = req.login_domain.indexOf("keytrain") !== -1 ? 1 : 2;

  const initialState = {
    root: {
      messages: [],
      site_type: site_type,
      self_signup: self_signup,
      user_data: user_data
    }
  };

  console.log("initialState", initialState);
  const store = configureStore(initialState);
  // console.log(store)
  match({ routes, location: req.url }, (
    error,
    redirectLocation,
    renderProps
  ) => {
    if (error) {
      res.status(500);
      res.send(error.message);
    } else if (redirectLocation) {
      res.redirect(302, redirectLocation.pathname + redirectLocation.search);
    } else if (renderProps) {
      const renderedContent = renderToString(
        <Provider store={store}>
          <RouterContext {...renderProps} />
        </Provider>
      );

      consolidate.handlebars(
        "src/client/index.handlebars",
        {
          html: renderedContent,
          state: JSON.stringify(initialState),
          keytrain_site: site_type == 1 ? true : false,
          self_signup: self_signup
        },
        (error, html) => {
          if (error) {
            console.error(error);
            throw error;
          }

          res.status(200);
          res.send(html);
        }
      );
    }
  });
});

app.listen(port, () => {
  console.log(
    `Listening on port ${port}. Open up http://localhost:${port}/ in your browser`
  );
});
