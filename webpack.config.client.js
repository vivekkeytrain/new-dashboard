const ExtractTextPlugin = require("extract-text-webpack-plugin");
const path = require("path");
const R = require("ramda");
const webpack = require("webpack");
// const WebpackDevServer = require('webpack-dev-server')
const isProduction = process.env.NODE_ENV === "production";
const isDevelopment = !isProduction;
const createEnvAwareArray = R.reject(R.isNil);

const ifProduction = x => isProduction ? x : null;
const ifDevelopment = x => isDevelopment ? x : null;

module.exports = {
  name: "client-side code",
  entry: createEnvAwareArray([
    // ifDevelopment("webpack-dev-server/client?http://localhost:3000"),
    //ifDevelopment("webpack-hot-middleware/client"),
    path.join(__dirname, "src", "client")
  ]),
  output: {
    path: path.join(__dirname, "public"),
    publicPath: "/",
    filename: "bundle.js"
  },
  devtool: "source-map",
  // devServer: {
  //   inline: true
  // },
  plugins: createEnvAwareArray([
    new webpack.optimize.OccurrenceOrderPlugin(),
    ifDevelopment(new webpack.HotModuleReplacementPlugin()),
    ifProduction(new webpack.DefinePlugin({
      "process.env.NODE_ENV": '"production"'
    })),
    ifProduction(new webpack.optimize.UglifyJsPlugin()),
    // ifProduction(new ExtractTextPlugin('styles.css'))
    ifProduction(new ExtractTextPlugin({
      filename: "css/styles.css",
      allChunks: true
    }))
  ]),
  resolve: {
    modules: [__dirname, "node_modules"]
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        loaders: "babel-loader",
        exclude: /node_modules/,
        query: {
          presets: ["es2015-webpack", "stage-2", "react"],
          plugins: ["transform-inline-environment-variables"]
        }
      },
      // {
      //   test: /\.js$/,
      //   loaders: [
      //     ifDevelopment("react-hot"),
      //     "babel?presets[]=es2015-webpack,presets[]=stage-2,presets[]=react,plugins[]=transform-inline-environment-variables"
      //   ]
      // },
      {
        test: /\.scss$/,
        loaders: isProduction ? ExtractTextPlugin.extract({
                fallbackLoader: "style-loader",
                loader: ["css", "sass"]
              }) : ["style", "css", "sass"]
      },
      // {
      //   test: /\.scss$/,
      //   loaders: ["style", "css", "sass"]
      // },
      {
        test: /\.woff(2)?(\?v=\d+\.\d+\.\d+)?$/,
        loader: "url?limit=10000&minetype=application/font-woff"
      },
      {
        test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
        loader: "url?limit=10000&minetype=application/octet-stream"
      },
      {
        test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
        loader: "file"
      },
      {
        test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
        loader: "url?limit=10000&minetype=image/svg+xml"
      }
    ]
  }
};
